
package example

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._

import org.apache.spark.rdd.RDD


object Example {
  val conf: SparkConf = new SparkConf().setAppName("example").setMaster("local")

  val sc: SparkContext = new SparkContext(conf)

  def sumOfPlusOnes = sc.parallelize(List(1, 2, 3, 4, 5)).map(_+1).reduce(_+_)
}

object WordCount extends App{
  val conf: SparkConf = new SparkConf().setMaster("local[*]").setAppName("example")
    .set("spark.driver.host", "localhost")
  val sc: SparkContext = new SparkContext(conf)
  import java.io.File
  def filePath = {
    new File(this.getClass.getClassLoader.getResource("example/book.txt").toURI).getPath
  }
  val bookRdd = sc.textFile(filePath)

  // Split into words separated by a space character
  val words = bookRdd.flatMap(x => x.split(" "))

  // Count up the occurrences of each word
  //val wordCounts = words.countByValue()
  val wordCounts = words.map(word =>(word,1)).reduceByKey(_+_)

  // Print the results.
  wordCounts.foreach(println)

  sc.stop()

}

