Big Data Analysis With Scala and Spark

## Highlights

- Getting started , Spark and Basics - `Example - WordCount and Assignment - Wikipedia`
- Reduction Operations & Distributed Key-Value Pairs - `Example K-Means and Assignment - StackOverflow`
- SQL, Dataframes, and Datasets - `aggregate,RDD vs dataset`


# Reference

- [Coursera Big Data Analysis with Scala and Spark](https://www.coursera.org/learn/scala-spark-big-data/home/info)`EPFL`
